/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/css-loader/dist/cjs.js!./src/public/css/style.css":
/*!************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/public/css/style.css ***!
  \************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ \"./node_modules/css-loader/dist/runtime/api.js\");\n/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/getUrl.js */ \"./node_modules/css-loader/dist/runtime/getUrl.js\");\n/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _fonts_space_age_ttf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fonts/space_age.ttf */ \"./src/public/css/fonts/space_age.ttf\");\n/* harmony import */ var _fonts_nasalization_rg_ttf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fonts/nasalization-rg.ttf */ \"./src/public/css/fonts/nasalization-rg.ttf\");\n/* harmony import */ var _fonts_xirod_ttf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./fonts/xirod.ttf */ \"./src/public/css/fonts/xirod.ttf\");\n/* harmony import */ var _img_meteors_jpg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../img/meteors.jpg */ \"./src/public/img/meteors.jpg\");\n// Imports\n\n\n\n\n\n\nvar ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});\nvar ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()(_fonts_space_age_ttf__WEBPACK_IMPORTED_MODULE_2__);\nvar ___CSS_LOADER_URL_REPLACEMENT_1___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()(_fonts_nasalization_rg_ttf__WEBPACK_IMPORTED_MODULE_3__);\nvar ___CSS_LOADER_URL_REPLACEMENT_2___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()(_fonts_xirod_ttf__WEBPACK_IMPORTED_MODULE_4__);\nvar ___CSS_LOADER_URL_REPLACEMENT_3___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()(_img_meteors_jpg__WEBPACK_IMPORTED_MODULE_5__);\n// Module\n___CSS_LOADER_EXPORT___.push([module.id, \"@font-face {\\r\\n\\tfont-family: space;\\r\\n\\tsrc: url(\" + ___CSS_LOADER_URL_REPLACEMENT_0___ + \");\\r\\n}\\r\\n\\r\\n@font-face {\\r\\n\\tfont-family: nasalization;\\r\\n\\tsrc: url(\" + ___CSS_LOADER_URL_REPLACEMENT_1___ + \");\\r\\n}\\r\\n\\r\\n@font-face {\\r\\n\\tfont-family: xirod;\\r\\n\\tsrc: url(\" + ___CSS_LOADER_URL_REPLACEMENT_2___ + \");\\r\\n}\\r\\n\\r\\nbody {\\r\\n\\tbackground-color: #081F1F;\\r\\n\\tbackground-image: url(\" + ___CSS_LOADER_URL_REPLACEMENT_3___ + \");\\r\\n\\tbackground-size: cover;\\r\\n\\tbackground-blend-mode: screen;\\r\\n\\tcolor: silver;\\r\\n\\tfont-family: nasalization;\\r\\n\\tpadding: 0 5% 0 5%;\\r\\n}\\r\\n\\r\\nh1 {\\r\\n\\tfont-family: space;\\r\\n\\tfont-weight: bolder;\\r\\n\\tfont-size: 4.5em;\\r\\n}\\r\\n\\r\\nh2 {\\r\\n\\tfont-family: space;\\r\\n\\tfont-weight: bold;\\r\\n\\tfont-size: 2em;\\r\\n}\\r\\n\\r\\na:link {\\r\\n\\tcolor: grey;\\r\\n}\\r\\n\\r\\n#map {\\r\\n\\theight: 400px;\\r\\n\\twidth: 100%;\\r\\n\\tborder: 1px solid;\\r\\n}\\r\\n\\r\\ninput, input[type=submit], select {\\r\\n\\tbackground-color: #2F4F4F !important;\\r\\n\\tcolor: lightgray;\\r\\n\\tborder: 1px solid;\\r\\n}\\r\\n\\r\\n.xirod {\\r\\n\\tfont-family: xirod;\\r\\n\\tfont-size: 0.8em;\\r\\n}\\r\\n\\r\\n.content {\\r\\n\\tbackground-color: #081F1F;\\r\\n\\tborder: 1px solid;\\r\\n\\tpadding: 5px;\\r\\n\\twidth: 100%;\\r\\n}\\r\\n\\r\\n.icon {\\r\\n\\twidth: 50px;\\r\\n\\tborder-radius: 50%;\\r\\n\\topacity: 50%;\\r\\n}\\r\\n\\r\\n\", \"\"]);\n// Exports\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);\n\n\n//# sourceURL=webpack://my-webpack-project/./src/public/css/style.css?./node_modules/css-loader/dist/cjs.js");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";
eval("\n\n/*\n  MIT License http://www.opensource.org/licenses/mit-license.php\n  Author Tobias Koppers @sokra\n*/\n// css base code, injected by the css-loader\n// eslint-disable-next-line func-names\nmodule.exports = function (cssWithMappingToString) {\n  var list = []; // return the list of modules as css string\n\n  list.toString = function toString() {\n    return this.map(function (item) {\n      var content = cssWithMappingToString(item);\n\n      if (item[2]) {\n        return \"@media \".concat(item[2], \" {\").concat(content, \"}\");\n      }\n\n      return content;\n    }).join(\"\");\n  }; // import a list of modules into the list\n  // eslint-disable-next-line func-names\n\n\n  list.i = function (modules, mediaQuery, dedupe) {\n    if (typeof modules === \"string\") {\n      // eslint-disable-next-line no-param-reassign\n      modules = [[null, modules, \"\"]];\n    }\n\n    var alreadyImportedModules = {};\n\n    if (dedupe) {\n      for (var i = 0; i < this.length; i++) {\n        // eslint-disable-next-line prefer-destructuring\n        var id = this[i][0];\n\n        if (id != null) {\n          alreadyImportedModules[id] = true;\n        }\n      }\n    }\n\n    for (var _i = 0; _i < modules.length; _i++) {\n      var item = [].concat(modules[_i]);\n\n      if (dedupe && alreadyImportedModules[item[0]]) {\n        // eslint-disable-next-line no-continue\n        continue;\n      }\n\n      if (mediaQuery) {\n        if (!item[2]) {\n          item[2] = mediaQuery;\n        } else {\n          item[2] = \"\".concat(mediaQuery, \" and \").concat(item[2]);\n        }\n      }\n\n      list.push(item);\n    }\n  };\n\n  return list;\n};\n\n//# sourceURL=webpack://my-webpack-project/./node_modules/css-loader/dist/runtime/api.js?");

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/getUrl.js":
/*!********************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/getUrl.js ***!
  \********************************************************/
/***/ ((module) => {

"use strict";
eval("\n\nmodule.exports = function (url, options) {\n  if (!options) {\n    // eslint-disable-next-line no-param-reassign\n    options = {};\n  } // eslint-disable-next-line no-underscore-dangle, no-param-reassign\n\n\n  url = url && url.__esModule ? url.default : url;\n\n  if (typeof url !== \"string\") {\n    return url;\n  } // If url is already wrapped in quotes, remove them\n\n\n  if (/^['\"].*['\"]$/.test(url)) {\n    // eslint-disable-next-line no-param-reassign\n    url = url.slice(1, -1);\n  }\n\n  if (options.hash) {\n    // eslint-disable-next-line no-param-reassign\n    url += options.hash;\n  } // Should url be wrapped?\n  // See https://drafts.csswg.org/css-values-3/#urls\n\n\n  if (/[\"'() \\t\\n]/.test(url) || options.needQuotes) {\n    return \"\\\"\".concat(url.replace(/\"/g, '\\\\\"').replace(/\\n/g, \"\\\\n\"), \"\\\"\");\n  }\n\n  return url;\n};\n\n//# sourceURL=webpack://my-webpack-project/./node_modules/css-loader/dist/runtime/getUrl.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************************!*\
  !*** ./src/index.js + 1 modules ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("// ESM COMPAT FLAG\n__webpack_require__.r(__webpack_exports__);\n\n// EXTERNAL MODULE: ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\nvar injectStylesIntoStyleTag = __webpack_require__(\"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\nvar injectStylesIntoStyleTag_default = /*#__PURE__*/__webpack_require__.n(injectStylesIntoStyleTag);\n// EXTERNAL MODULE: ./node_modules/css-loader/dist/cjs.js!./src/public/css/style.css\nvar style = __webpack_require__(\"./node_modules/css-loader/dist/cjs.js!./src/public/css/style.css\");\n;// CONCATENATED MODULE: ./src/public/css/style.css\n\n            \n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = injectStylesIntoStyleTag_default()(style.default, options);\n\n\n\n/* harmony default export */ const css_style = (style.default.locals || {});\n// EXTERNAL MODULE: ./src/public/js/map.js\nvar map = __webpack_require__(\"./src/public/js/map.js\");\n;// CONCATENATED MODULE: ./src/index.js\n\r\n\r\n\r\nwindow.charge = map.default;\r\nwindow.sendMeteor = map.sendMeteor;\n\n//# sourceURL=webpack://my-webpack-project/./src/index.js_+_1_modules?");

/***/ }),

/***/ "./src/public/js/entry.js":
/*!********************************!*\
  !*** ./src/public/js/entry.js ***!
  \********************************/
/***/ (() => {

eval("//const apiPath = \"http://localhost:3376/game\";\r\n\n\n//# sourceURL=webpack://my-webpack-project/./src/public/js/entry.js?");

/***/ }),

/***/ "./src/public/js/form.js":
/*!*******************************!*\
  !*** ./src/public/js/form.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./map.js */ \"./src/public/js/map.js\");\n/* provided dependency */ var $ = __webpack_require__(/*! jquery */ \"jquery\");\n/* eslint-env jquery */\r\n\r\n// MàJ de l'indicateur numérique du zoom\r\nfunction updateZoomValue() {\r\n    $('#zoomValue').html($('#zoom').val());\r\n    (0,_map_js__WEBPACK_IMPORTED_MODULE_0__.default)();\r\n}\r\n\r\n\r\n// Abonnement aux événements de changement\r\n$('#lat').change(_map_js__WEBPACK_IMPORTED_MODULE_0__.default);\r\n$('#lon').change(_map_js__WEBPACK_IMPORTED_MODULE_0__.default);\r\n$('#zoom').change(updateZoomValue);\r\n\n\n//# sourceURL=webpack://my-webpack-project/./src/public/js/form.js?");

/***/ }),

/***/ "./src/public/js/map.js":
/*!******************************!*\
  !*** ./src/public/js/map.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"mymap\": () => (/* binding */ mymap),\n/* harmony export */   \"sendMeteor\": () => (/* binding */ sendMeteor),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n// initialisation de la map\r\nlet lat = 45.782, lng = 4.8656, zoom = 19;\r\n\r\nlet game=false;\r\n\r\nlet composition;\r\nlet idm;\r\nlet ttl;\r\n\r\nvar L = window.L;\r\n\r\nlet mymap = L.map('map', {\r\n    center: [lat, lng],\r\n    zoom: zoom\r\n});\r\n\r\n\r\n\r\n// Icon options\r\nvar iconOptions = {\r\n    iconUrl: 'http://pngimg.com/uploads/meteor/small/meteor_PNG22.png',\r\n    iconSize: [50, 50]\r\n}\r\n \r\n// Creating a custom icon\r\nvar customIcon = L.icon(iconOptions);\r\n\r\nvar markerOptions = {\r\n    title: \"MyLocation\",\r\n    clickable: true,\r\n    draggable: false,\r\n    icon: customIcon\r\n}\r\n//updateMap();\r\n\r\n// Création d'un \"tile layer\" (permet l'affichage sur la carte)\r\nL.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}@2x.jpg90?access_token=pk.eyJ1IjoibTFpZjEzIiwiYSI6ImNqczBubmhyajFnMnY0YWx4c2FwMmRtbm4ifQ.O6W7HeTW3UvOVgjCiPrdsA', {\r\n    maxZoom: 22,\r\n    minZoom: 1,\r\n    attribution: 'Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, ' +\r\n\t'<a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, ' +\r\n\t'Imagery © <a href=\"https://www.mapbox.com/\">Mapbox</a>',\r\n    id: 'mapbox/streets-v11',\r\n    tileSize: 512,\r\n    zoomOffset: -1,\r\n    accessToken: 'pk.eyJ1IjoibTFpZjEzIiwiYSI6ImNqczBubmhyajFnMnY0YWx4c2FwMmRtbm4ifQ.O6W7HeTW3UvOVgjCiPrdsA'\r\n}).addTo(mymap);\r\n\r\n// Ajout d'un marker\r\nL.marker([45.78207, 4.86559]).addTo(mymap).bindPopup('Entrée du bâtiment<br><strong>Nautibus</strong>.').openPopup();\r\n\r\n\r\n// Ajout du rectangle vide\r\nL.polygon([\r\n    [45.781724, 4.864883],\r\n    [45.78445, 4.864883],\r\n    [45.787278, 4.888739],\r\n    [45.784883, 4.888739]\r\n],{color: \"#ff7800\", weight: 1}).addTo(mymap);\r\n\r\n// Clic sur la carte\r\nvar popup = L.popup();\r\n\r\nmymap.on('click', e => {\r\n    lat = e.latlng.lat;\r\n    lng = e.latlng.lng;\r\n    popup.setLatLng(e.latlng).setContent(\"You clicked the map at \" + e.latlng.toString()).openOn(mymap);\r\n    if(game == true){\r\n        //if(lat<45.781724 && lng<4.864883 || lat>45.78445 && lng<4.864883 \r\n        //\t|| lat>45.787278 && lng>4.888739 || lat<45.784883 && lng>4.888739){\r\n        L.marker([lat, lng],markerOptions).addTo(mymap).bindPopup(composition).openPopup();\r\n        postMeteorite(idm,ttl,lat,lng,composition);\r\n        game = false;\r\n    }//}\r\n    updateMap();\r\n});\r\n\r\ndocument.getElementById(\"setMeteorType\").addEventListener(\"button\", sendMeteor);\r\n// météorite \r\nfunction sendMeteor(){\t\r\n    alert(\"prêt pour l'impact\")\r\n    game = true;\r\n    composition = document.getElementById('pet-select').value;\r\n   idm = document.getElementById('idm').value;\r\n    ttl = document.getElementById('ttl').value;\r\n}\r\n\r\n\r\n\r\n// Mise à jour de la map\r\nfunction updateMap() {\r\n    // Affichage à la nouvelle position\r\n    mymap.setView([lat, lng], zoom);\r\n\r\n    // La fonction de validation du formulaire renvoie false pour bloquer le rechargement de la page.\r\n    return false;\r\n}\r\n\r\nfunction charge(){\r\n    console.log(\"salut\");\r\n    let url = \"http://192.168.75.107:3376/game\";\r\n    let init = {\r\n        method: 'GET',\r\n        headers: { 'Accept': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost', 'Origin': 'http://localhost' },\r\n        mode: 'cors',\r\n        cache: 'default'\r\n    };\r\n\r\n    let request = url;\r\n\r\n    fetch(request, init)\r\n        .then(res => res.json())\r\n        .then(objmap => {console.log(objmap);\r\n            var li = document.getElementById(\"list-users\");\r\n            for(var i in objmap){\r\n                //affichageUser(users[i],li);\r\n                let tabicon =[];\r\n                let tabcustomicon = [];\r\n                let tabmarkericon = [];\r\n                console.log(objmap[i].length);\r\n                for(let j=0; j<objmap[i].length; j++){\r\n                    if(i == \"users\"){\r\n                        // affichage joueur sur map et liste\r\n                        var iconOptionsuser = {\r\n                            iconUrl: objmap[i][j].image,\r\n                            iconSize: [50, 50]\r\n                        }\r\n                        tabicon.push(iconOptionsuser);\r\n                         \r\n                        // Creating a custom icon\r\n                        //var customIcon = L.icon(iconOptions);\r\n                        tabcustomicon.push(L.icon(iconOptionsuser));\r\n                        \r\n                        var markerOptionsuser = {\r\n                            title: \"MyLocation\",\r\n                            clickable: true,\r\n                            draggable: false,\r\n                            icon: tabcustomicon[j]\r\n                        }\r\n\r\n                        tabmarkericon.push(markerOptionsuser);\r\n                        affichageUser(objmap[i],j,li);\r\n                        L.marker([objmap[i][j].position[0], objmap[i][j].position[1]],tabmarkericon[j]).addTo(mymap).bindPopup(`Utilisateur <strong>${objmap[i][j].survivant}</strong><br>TTL: <strong>${objmap[i][j].TTL}</strong>s.`).openPopup();\r\n                        console.log(i);\r\n                    } else {\r\n                        // affichage météorite sur map \r\n                        L.marker([objmap[i][j].impact[0], objmap[i][j].impact[1]],markerOptions).addTo(mymap).bindPopup(objmap[i][j].composition).openPopup();\r\n                    }\r\n                }\r\n            }\r\n        })\r\n}/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (charge);\r\n\r\n\r\nfunction postMeteorite(id,ttl,x,y,composition){\r\n    fetch('http://192.168.75.107:3376/game/api/resources/meteorites', {\r\n        method: 'POST',\r\n        headers: { 'Content-Type': 'application/json' },\r\n        mode: 'cors',\r\n        body: JSON.stringify({\r\n            id:id,\r\n             ttl: ttl,\r\n            x:x,\r\n            y:y,\r\n            composition: composition\r\n        })\r\n    })\r\n}\r\n\r\nfunction affichageUser(users,j,li){\r\n    return li.innerHTML += `<li>\r\n                            <a href=\"javascript:updateImage(\"J1\");\"><img src=${users[j].image} class=\"icon\"></a>&nbsp;&nbsp;-&nbsp;&nbsp;\r\n                            <a href=\"javascript:updateName(${users[j].survivant});\">${users[j].survivant}</a>&nbsp;&nbsp;-&nbsp;&nbsp;\r\n                            <strong>TTL</strong> : ${users[j].TTL}&nbsp;&nbsp;-&nbsp;&nbsp;\r\n                            <strong>Trophys</strong> : none\r\n                            </li>`;\r\n}\n\n//# sourceURL=webpack://my-webpack-project/./src/public/js/map.js?");

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("\n\nvar isOldIE = function isOldIE() {\n  var memo;\n  return function memorize() {\n    if (typeof memo === 'undefined') {\n      // Test for IE <= 9 as proposed by Browserhacks\n      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805\n      // Tests for existence of standard globals is to allow style-loader\n      // to operate correctly into non-standard environments\n      // @see https://github.com/webpack-contrib/style-loader/issues/177\n      memo = Boolean(window && document && document.all && !window.atob);\n    }\n\n    return memo;\n  };\n}();\n\nvar getTarget = function getTarget() {\n  var memo = {};\n  return function memorize(target) {\n    if (typeof memo[target] === 'undefined') {\n      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself\n\n      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {\n        try {\n          // This will throw an exception if access to iframe is blocked\n          // due to cross-origin restrictions\n          styleTarget = styleTarget.contentDocument.head;\n        } catch (e) {\n          // istanbul ignore next\n          styleTarget = null;\n        }\n      }\n\n      memo[target] = styleTarget;\n    }\n\n    return memo[target];\n  };\n}();\n\nvar stylesInDom = [];\n\nfunction getIndexByIdentifier(identifier) {\n  var result = -1;\n\n  for (var i = 0; i < stylesInDom.length; i++) {\n    if (stylesInDom[i].identifier === identifier) {\n      result = i;\n      break;\n    }\n  }\n\n  return result;\n}\n\nfunction modulesToDom(list, options) {\n  var idCountMap = {};\n  var identifiers = [];\n\n  for (var i = 0; i < list.length; i++) {\n    var item = list[i];\n    var id = options.base ? item[0] + options.base : item[0];\n    var count = idCountMap[id] || 0;\n    var identifier = \"\".concat(id, \" \").concat(count);\n    idCountMap[id] = count + 1;\n    var index = getIndexByIdentifier(identifier);\n    var obj = {\n      css: item[1],\n      media: item[2],\n      sourceMap: item[3]\n    };\n\n    if (index !== -1) {\n      stylesInDom[index].references++;\n      stylesInDom[index].updater(obj);\n    } else {\n      stylesInDom.push({\n        identifier: identifier,\n        updater: addStyle(obj, options),\n        references: 1\n      });\n    }\n\n    identifiers.push(identifier);\n  }\n\n  return identifiers;\n}\n\nfunction insertStyleElement(options) {\n  var style = document.createElement('style');\n  var attributes = options.attributes || {};\n\n  if (typeof attributes.nonce === 'undefined') {\n    var nonce =  true ? __webpack_require__.nc : 0;\n\n    if (nonce) {\n      attributes.nonce = nonce;\n    }\n  }\n\n  Object.keys(attributes).forEach(function (key) {\n    style.setAttribute(key, attributes[key]);\n  });\n\n  if (typeof options.insert === 'function') {\n    options.insert(style);\n  } else {\n    var target = getTarget(options.insert || 'head');\n\n    if (!target) {\n      throw new Error(\"Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.\");\n    }\n\n    target.appendChild(style);\n  }\n\n  return style;\n}\n\nfunction removeStyleElement(style) {\n  // istanbul ignore if\n  if (style.parentNode === null) {\n    return false;\n  }\n\n  style.parentNode.removeChild(style);\n}\n/* istanbul ignore next  */\n\n\nvar replaceText = function replaceText() {\n  var textStore = [];\n  return function replace(index, replacement) {\n    textStore[index] = replacement;\n    return textStore.filter(Boolean).join('\\n');\n  };\n}();\n\nfunction applyToSingletonTag(style, index, remove, obj) {\n  var css = remove ? '' : obj.media ? \"@media \".concat(obj.media, \" {\").concat(obj.css, \"}\") : obj.css; // For old IE\n\n  /* istanbul ignore if  */\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = replaceText(index, css);\n  } else {\n    var cssNode = document.createTextNode(css);\n    var childNodes = style.childNodes;\n\n    if (childNodes[index]) {\n      style.removeChild(childNodes[index]);\n    }\n\n    if (childNodes.length) {\n      style.insertBefore(cssNode, childNodes[index]);\n    } else {\n      style.appendChild(cssNode);\n    }\n  }\n}\n\nfunction applyToTag(style, options, obj) {\n  var css = obj.css;\n  var media = obj.media;\n  var sourceMap = obj.sourceMap;\n\n  if (media) {\n    style.setAttribute('media', media);\n  } else {\n    style.removeAttribute('media');\n  }\n\n  if (sourceMap && typeof btoa !== 'undefined') {\n    css += \"\\n/*# sourceMappingURL=data:application/json;base64,\".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), \" */\");\n  } // For old IE\n\n  /* istanbul ignore if  */\n\n\n  if (style.styleSheet) {\n    style.styleSheet.cssText = css;\n  } else {\n    while (style.firstChild) {\n      style.removeChild(style.firstChild);\n    }\n\n    style.appendChild(document.createTextNode(css));\n  }\n}\n\nvar singleton = null;\nvar singletonCounter = 0;\n\nfunction addStyle(obj, options) {\n  var style;\n  var update;\n  var remove;\n\n  if (options.singleton) {\n    var styleIndex = singletonCounter++;\n    style = singleton || (singleton = insertStyleElement(options));\n    update = applyToSingletonTag.bind(null, style, styleIndex, false);\n    remove = applyToSingletonTag.bind(null, style, styleIndex, true);\n  } else {\n    style = insertStyleElement(options);\n    update = applyToTag.bind(null, style, options);\n\n    remove = function remove() {\n      removeStyleElement(style);\n    };\n  }\n\n  update(obj);\n  return function updateStyle(newObj) {\n    if (newObj) {\n      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {\n        return;\n      }\n\n      update(obj = newObj);\n    } else {\n      remove();\n    }\n  };\n}\n\nmodule.exports = function (list, options) {\n  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>\n  // tags it will allow on a page\n\n  if (!options.singleton && typeof options.singleton !== 'boolean') {\n    options.singleton = isOldIE();\n  }\n\n  list = list || [];\n  var lastIdentifiers = modulesToDom(list, options);\n  return function update(newList) {\n    newList = newList || [];\n\n    if (Object.prototype.toString.call(newList) !== '[object Array]') {\n      return;\n    }\n\n    for (var i = 0; i < lastIdentifiers.length; i++) {\n      var identifier = lastIdentifiers[i];\n      var index = getIndexByIdentifier(identifier);\n      stylesInDom[index].references--;\n    }\n\n    var newLastIdentifiers = modulesToDom(newList, options);\n\n    for (var _i = 0; _i < lastIdentifiers.length; _i++) {\n      var _identifier = lastIdentifiers[_i];\n\n      var _index = getIndexByIdentifier(_identifier);\n\n      if (stylesInDom[_index].references === 0) {\n        stylesInDom[_index].updater();\n\n        stylesInDom.splice(_index, 1);\n      }\n    }\n\n    lastIdentifiers = newLastIdentifiers;\n  };\n};\n\n//# sourceURL=webpack://my-webpack-project/./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js?");

/***/ }),

/***/ "./src/public/css/fonts/nasalization-rg.ttf":
/*!**************************************************!*\
  !*** ./src/public/css/fonts/nasalization-rg.ttf ***!
  \**************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"94fec615f5ca38689174.ttf\";\n\n//# sourceURL=webpack://my-webpack-project/./src/public/css/fonts/nasalization-rg.ttf?");

/***/ }),

/***/ "./src/public/css/fonts/space_age.ttf":
/*!********************************************!*\
  !*** ./src/public/css/fonts/space_age.ttf ***!
  \********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"1ad6f4f0cb975b88b5b9.ttf\";\n\n//# sourceURL=webpack://my-webpack-project/./src/public/css/fonts/space_age.ttf?");

/***/ }),

/***/ "./src/public/css/fonts/xirod.ttf":
/*!****************************************!*\
  !*** ./src/public/css/fonts/xirod.ttf ***!
  \****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"bc1b09954920935b481b.ttf\";\n\n//# sourceURL=webpack://my-webpack-project/./src/public/css/fonts/xirod.ttf?");

/***/ }),

/***/ "./src/public/img/meteors.jpg":
/*!************************************!*\
  !*** ./src/public/img/meteors.jpg ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
eval("module.exports = __webpack_require__.p + \"337d0e73a09e8840cd0f.jpg\";\n\n//# sourceURL=webpack://my-webpack-project/./src/public/img/meteors.jpg?");

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = jQuery;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	__webpack_require__("./src/index.js");
/******/ 	__webpack_require__("./src/public/js/entry.js");
/******/ 	__webpack_require__("./src/public/js/map.js");
/******/ 	var __webpack_exports__ = __webpack_require__("./src/public/js/form.js");
/******/ 	
/******/ })()
;