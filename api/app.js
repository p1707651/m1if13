const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();
var bodyParser = require('body-parser');
var cors = require('cors');

var adminRoute = require('./routes/adminRoute');
var jeuRoute = require('./routes/jeuRoute');
//var resources = require('./ressources/*');
const user = require('./ressources/user');
const meteorite = require('./ressources/meteorite');
const ZRR = require('./ressources/ZRR');

let users = new user();
users.newUser("1","Jonathan","https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png",45.78257, 4.866461,180,"player");
users.newUser("2","Kader","https://www.nicepng.com/png/detail/922-9225032_rouge-effet-de-lumire-toile-halo-png-et.png",45.782393, 4.868414,180,"player");
let meteorites = new meteorite();
meteorites.newMeteorite("1",180,45.782166,4.866686,"Astra-Z");
let ZRRs = new ZRR("0","1","2","3");
let GameMap = {users:users,meteorites:meteorites};

//projet suite
let apiPath;

//permet d'utiliser render avec le module html
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

router.get('/',function(req,res){
  res.send({users:users.getAll(),meteorites:meteorites.getAll()});
});

router.get('/api',function(req,res){
  res.sendFile(path.join(__dirname+'/index.html'));
  //__dirname : It will resolve to your project folder.
});

router.get('/public',function(req,res){
  res.sendFile(path.join(__dirname+'/public/about.html'));
});

router.get('/admin',function(req,res){
  res.sendFile(path.join(__dirname+'/dist/index.html'));
});

router.get('/bundle.js',function(req,res){
  res.sendFile(path.join(__dirname+'/dist/bundle.js'));
});

router.get('/1ad6f4f0cb975b88b5b9.ttf',function(req,res){
  res.sendFile(path.join(__dirname+'/dist/1ad6f4f0cb975b88b5b9.ttf'));
});

router.get('/bc1b09954920935b481b.ttf',function(req,res){
  res.sendFile(path.join(__dirname+'/dist/bc1b09954920935b481b.ttf'));
});

router.get('/94fec615f5ca38689174.ttf',function(req,res){
  res.sendFile(path.join(__dirname+'/dist/94fec615f5ca38689174.ttf'));
});

router.get('/337d0e73a09e8840cd0f.jpg',function(req,res){
  res.sendFile(path.join(__dirname+'/dist/337d0e73a09e8840cd0f.jpg'));
});

router.get('/api/resources',function(req,res){
  res.send(GameMap);
});

router.get('/api/resources/users',function(req,res){
  res.send({users:users.getAll()});
});

router.get('/api/resources/meteorites',function(req,res){
  res.send({meteorites:meteorites.getAll()});
});

router.get('/api/resources/meteorite/:id',function(req,res){
  let id = req.params.id;

  if (meteorites.getMeteorite(id).length !== 0){
      res.send(meteorites.getMeteorite(id));
      //res.status(204).send('user');
  } else {
      res.status(404).send('meteorite not found');
  }
  //res.send({meteorites.getMeteorite(id)});
});

router.post('/api/resources/meteorite/:id', function (req, res){
  let id = req.params.id;

  if (meteorites.getMeteorite(id).length !== 0){
      res.send(meteorites.delMeteorite(id));
      //res.status(204).send('user');
  } else {
      res.status(404).send('meteorite not found');
  }
  //res.send({meteorites.getMeteorite(id)});
});

router.put('/api/resources/:resourceId/image', bodyParser.json(),function (req, res) {
  //TODO CHANGE SOMETHING FOR URL OBJECT (MAYBE LAUNCH AN API OF AN EXISTING WEBSITE)

  let id = req.params.resourceId;
  let url = req.body.url;

  if (id !== undefined){
      if (url.endsWith('.png') || url.endsWith('.jpg') || url.endsWith('.jpeg')){
          users.updateImageUser(id,url);
          res.status(204).send('Update');
      } else {
          res.status(400).send('URL Invalide');
      }
      console.log(users.getUser(id));
  } else {
      res.status(404).send('user not found');
  }
});


router.put('/api/resources/:resourceId/position', bodyParser.json(), function (req, res) {
  let id = req.params.resourceId;
  let pos = req.body.position;

  if (id !== undefined){
    if (Number(pos[0]) && Number(pos[1])){
        if (pos[0] >= 0 && pos[1] >= 0){
            users.updatePositionUser(id,pos[0],pos[1]);
            res.status(204).send('Position modifier')
            console.log(users.getUser(id));
        } else {
              res.status(400).send('Position invalide')
        }
    } else {
        res.status(400).send('Position invalide')
    }
  } else {
      res.status(404).send('Utilisteur non existant')
  }
});

router.get(`/api/resources/user/:survivant`, bodyParser.json(), function (req, res) {
  let survivant = req.params.survivant;

  if (users.getUser(survivant).length !== 0){
      res.send(users.getUser(survivant));
      //res.status(204).send('user');
  } else {
      res.status(404).send('user not found');
  }
});

router.get(`/api/resources/user/:survivant/position`, bodyParser.json(), function (req, res) {
  let survivant = req.params.survivant;

  if (users.getUser(survivant).length !== 0){
      res.send(users.getPositionUser(survivant));
      //res.status(204).send('user');
  } else {
      res.status(404).send('user not found');
  }
});

router.post('/api/resources/meteorites', bodyParser.json(),function (req, res) {
  //var id = this.meteorites.getAll().length()+1;
  meteorites.newMeteorite(req.body.id,req.body.ttl,req.body.x,req.body.y,req.body.composition);
  console.log(req.body);
});

router.post('/api/resources/user', bodyParser.json(),function (req, res) {
  users.newUser(req.body.id,req.body.survivant,req.body.image,req.body.x, req.body.y,req.body.ttl,"player");
  console.log(req.body);
});



//add the router & static files
app.use(cors());
app.use('/game', router);
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
  extended: true
}));
//app.use(express.static('files'));

app.use('/admin', adminRoute)
app.use('/jeu', jeuRoute)
//app.use('/admin',dist)
//app.use('/api', resources)

//error handler
/*app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
  });*/
app.listen(process.env.port || 3376);

console.log('Running at Port 3376');

console.log(users);
console.log(meteorites);
console.log(ZRRs);