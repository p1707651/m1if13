class meteorite {
    constructor(id,ttl,x,y,composition){
        this.id = id;
        this.ttl = ttl;
        this.impact = [x,y];
        this.composition = composition;
    }
}

class meteorites {
    constructor(){
        this.meteorites = [];
    }

    newMeteorite(id,ttl,x,y,composition){
        let m = new meteorite(id,ttl,x,y,composition);
        this.meteorites.push(m);
    }

    getMeteorite(id){
        return this.meteorites.filter(function (n){ return n.id === id});
    }

    delMeteorite(id){
        return  this.meteorites = this.meteorites.filter(function (n){ return n.id !== id});
    }

    getAll(){
        return this.meteorites;
    }
}

module.exports = meteorite;
module.exports = meteorites;