class user {
    constructor(id,name,url,x,y,TTL,administrateur){
        this.id = id;
        this.survivant = name;
        this.image = url;
        this.position = [x,y];
        this.TTL = TTL;
        this.administrateur = administrateur;
    }
}

class users {
    constructor(){
        this.users = [];
    }

    newUser(id,name,url,x,y,TTL,administrateur){
        let u = new user(id,name,url,x,y,TTL,administrateur);
        this.users.push(u);
    }

    getUser(name){
        return this.users.filter(function (n){ return n.survivant === name});
    }

    getPositionUser(name){    
        let a = this.users.filter(
            function (n){ 
                return n.survivant === name
            }
        );
        
        console.log(a);
        return a[0].position;
    }

    getAll(){
        return this.users;
    }

    updateImageUser(name,url){    
        let a = this.users.filter(
            function (n){ 
                return n.id === name
            }
        );
        
        console.log(a);
        return a[0].image = url;
    }

    updatePositionUser(name,posx,posy){    
        let a = this.users.filter(
            function (n){ 
                return n.id === name
            }
        );
        
        console.log(a);
        return a[0].position = [posx,posy];
    }
}

module.exports = user;
module.exports = users;