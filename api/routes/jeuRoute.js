var express = require('express')
var router = express.Router()

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})
// define the home page route
router.get('/user', function (req, res) {
  res.send('User home page')
})
// define the user route
router.get('/user', function (req, res) {
  res.send('User')
})
router.get('/user/survivant', function (req, res) {
  res.send('survivant')
})

router.get('/meteorite', function (req, res) {
  res.send('meteorite ')
})

router.get('/meteorite/impact', function (req, res) {
    res.send('meteorite impact ')
})

router.get('/meteorite/composition', function (req, res) {
  res.send('meteorite composition')
})

router.get('/ZRR', function (req, res) {
  res.send('ZRR')
})


//error handler
/*app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
  });*/

module.exports = router