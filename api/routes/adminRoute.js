var express = require('express')
var router = express.Router()

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})
// define the home page route
router.get('/user/admin', function (req, res) {
  res.send('Admin home page')
})

//error handler
/*app.use(function (req, res, next) {
  res.status(404).send("Sorry can't find that!")
});*/

module.exports = router