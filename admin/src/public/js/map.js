// initialisation de la map
let lat = 45.782, lng = 4.8656, zoom = 19;

let game=false;

let composition;
let ttl;
let idm;

var L = window.L;

let mymap = L.map('map', {
    center: [lat, lng],
    zoom: zoom
});
export {mymap};


// Icon options
var iconOptions = {
    iconUrl: 'http://pngimg.com/uploads/meteor/small/meteor_PNG22.png',
    iconSize: [50, 50]
}
 
// Creating a custom icon
var customIcon = L.icon(iconOptions);

var markerOptions = {
    title: "MyLocation",
    clickable: true,
    draggable: false,
    icon: customIcon
}
//updateMap();

// Création d'un "tile layer" (permet l'affichage sur la carte)
L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}@2x.jpg90?access_token=pk.eyJ1IjoibTFpZjEzIiwiYSI6ImNqczBubmhyajFnMnY0YWx4c2FwMmRtbm4ifQ.O6W7HeTW3UvOVgjCiPrdsA', {
    maxZoom: 22,
    minZoom: 1,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
	'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
	'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibTFpZjEzIiwiYSI6ImNqczBubmhyajFnMnY0YWx4c2FwMmRtbm4ifQ.O6W7HeTW3UvOVgjCiPrdsA'
}).addTo(mymap);

// Ajout d'un marker
L.marker([45.78207, 4.86559]).addTo(mymap).bindPopup('Entrée du bâtiment<br><strong>Nautibus</strong>.').openPopup();


// Ajout du rectangle vide
L.polygon([
    [45.781724, 4.864883],
    [45.78445, 4.864883],
    [45.787278, 4.888739],
    [45.784883, 4.888739]
],{color: "#ff7800", weight: 1}).addTo(mymap);

// Clic sur la carte
var popup = L.popup();

mymap.on('click', e => {
    lat = e.latlng.lat;
    lng = e.latlng.lng;
    popup.setLatLng(e.latlng).setContent("You clicked the map at " + e.latlng.toString()).openOn(mymap);
    if(game == true){
        //if(lat<45.781724 && lng<4.864883 || lat>45.78445 && lng<4.864883 
        //	|| lat>45.787278 && lng>4.888739 || lat<45.784883 && lng>4.888739){
        L.marker([lat, lng],markerOptions).addTo(mymap).bindPopup(composition).openPopup();
        postMeteorite(ttl,lat,lng,composition);
        game = false;
    }//}
    updateMap();
});

document.getElementById("setMeteorType").addEventListener("button", sendMeteor);
// météorite 
function sendMeteor(){	
    alert("prêt pour l'impact")
    game = true;
    composition = document.getElementById('pet-select').value;
    ttl = document.getElementById('ttl').value;    
}

export {sendMeteor};

// Mise à jour de la map
function updateMap() {
    // Affichage à la nouvelle position
    mymap.setView([lat, lng], zoom);

    // La fonction de validation du formulaire renvoie false pour bloquer le rechargement de la page.
    return false;
}

function charge(){
    console.log("salut");
    let url = "http://192.168.75.107:3376/game";
    let init = {
        method: 'GET',
        headers: { 'Accept': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost', 'Origin': 'http://localhost' },
        mode: 'cors',
        cache: 'default'
    };

    let request = url;

    fetch(request, init)
        .then(res => res.json())
        .then(objmap => {console.log(objmap);
            var li = document.getElementById("list-users");
            for(var i in objmap){
                //affichageUser(users[i],li);
                let tabicon =[];
                let tabcustomicon = [];
                let tabmarkericon = [];
                console.log(objmap[i].length);
                for(let j=0; j<objmap[i].length; j++){
                    if(i == "users"){
                        // affichage joueur sur map et liste
                        var iconOptionsuser = {
                            iconUrl: objmap[i][j].image,
                            iconSize: [50, 50]
                        }
                        tabicon.push(iconOptionsuser);
                         
                        // Creating a custom icon
                        //var customIcon = L.icon(iconOptions);
                        tabcustomicon.push(L.icon(iconOptionsuser));
                        
                        var markerOptionsuser = {
                            title: "MyLocation",
                            clickable: true,
                            draggable: false,
                            icon: tabcustomicon[j]
                        }

                        tabmarkericon.push(markerOptionsuser);
                        affichageUser(objmap[i],j,li);
                        L.marker([objmap[i][j].position[0], objmap[i][j].position[1]],tabmarkericon[j]).addTo(mymap).bindPopup(`Utilisateur <strong>${objmap[i][j].survivant}</strong><br>TTL: <strong>${objmap[i][j].TTL}</strong>s.`).openPopup();
                        console.log(i);
                    } else {
                        // affichage météorite sur map 
                        L.marker([objmap[i][j].impact[0], objmap[i][j].impact[1]],markerOptions).addTo(mymap).bindPopup(objmap[i][j].composition).openPopup();
                    }
                }
            }
        })
}export default charge;


function postMeteorite(ttl,x,y,composition){
    fetch('http://192.168.75.107:3376/game/api/resources/meteorites', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors',
        body: JSON.stringify({
            ttl: ttl,
            x:x,
            y:y,
            composition: composition
        })
    })
}

function affichageUser(users,j,li){
    return li.innerHTML += `<li>
                            <a href="javascript:updateImage("J1");"><img src=${users[j].image} class="icon"></a>&nbsp;&nbsp;-&nbsp;&nbsp;
                            <a href="javascript:updateName(${users[j].survivant});">${users[j].survivant}</a>&nbsp;&nbsp;-&nbsp;&nbsp;
                            <strong>TTL</strong> : ${users[j].TTL}&nbsp;&nbsp;-&nbsp;&nbsp;
                            <strong>Trophys</strong> : none
                            </li>`;
}


