var request = require("request");

var apiPath = "http://localhost:3376/game";


describe("users", function () {

  it("Récupération des users", function () {
      console.log("récupération des users")
      var req = {
          uri: `${apiPath}/api/resources/users`,
          headers: { 'Accept': 'application/json' }
      };

      request.get(req, function (error, resp, body) {
          var u = JSON.parse(resp.body);
          expect(u.users.length).toBe(2);
      });

  });

  it("création d'un user (204)", function() {
    console.log("création d'un user (204)");
    var req = {
      uri: `${apiPath}//api/resources/user`,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
          id: 3,
          survivant: "Jean",
          image: "Astra-X",
          x: 76.48978,
          y: 47.468678,
          ttl: 60,
          administrateur : "player"
      })
    };

    request.post(req, function (error, resp, body) {
      expect(resp.statusCode).toBe(204);
    });

  });

});