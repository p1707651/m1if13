var request = require("request");

var apiPath = "http://localhost:3376/game";


describe("users", function () {

  it("Récupération des  meteorites", function () {
      console.log("Récupération des  meteorites");
      var req = {
          uri: `${apiPath}/api/resources/meteorites`,
          headers: { 'Accept': 'application/json' }
      };

      request.get(req, function (error, resp, body) {
          var m = JSON.parse(resp.body);
          expect(m.length).toBe(1);
      });

  });

  it("création d'un météorite (204)", function() {
    console.log("création d'un météorite (204)");
    var req = {
      uri: `${apiPath}/api/ressource/meteorite`,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        ttl: 60,
        x: 89.58214,
        y: 35.468678,
        composition: "Astra-X" 
      })
    };

    request.post(req, function (error, resp, body) {
      expect(resp.statusCode).toBe(204);
    });

  });

});