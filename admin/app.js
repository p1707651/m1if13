const express = require('express')
const app = express()
var bodyParser = require('body-parser');
const fetch = require('node-fetch');
const path = require('path');
const meteorite = require('../api/ressources/meteorite');
const port = 8000

//permet d'utiliser render avec le module html
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
  /*router.get('/',function(req,res){
    res.render(path.join(__dirname, '/public/index.html'),{users:users.getAll()});
  });*/

app.get('/game', bodyParser.json(),function (req, res) {
    //TODO CHANGE SOMETHING FOR URL OBJECT (MAYBE LAUNCH AN API OF AN EXISTING WEBSITE)
  
    let url = "http://localhost:3376/game";
    let init = {
        method: 'GET',
        headers: { 'Accept': 'application/json', 'Access-Control-Allow-Origin': 'http://localhost', 'Origin': 'http://localhost' },
        mode: 'cors',
        cache: 'default'
    };

    let request = url;

    fetch(request, init)
        .then(res => res.json())
        .then(users => {console.log(users);res.render(path.join(__dirname, 'src/public/index.html'),users)})
  })

  
// SPA
app.use('/game', express.static('src/public'))

// 404 error pages
app.use(function (req, res, next) {
    res.status(404).send("This page does not exists")
})

