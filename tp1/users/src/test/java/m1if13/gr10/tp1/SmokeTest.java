package m1if13.gr10.tp1;

import m1if13.gr10.tp1.controller.UserRestControleur;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
public class SmokeTest {
    @Autowired
    private UserRestControleur controller;

    @Test
    void contextLoads() {
        assertThat(controller).isNotNull();
    }
}
