package m1if13.gr10.tp1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import m1if13.gr10.tp1.controller.UserRestControleur;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class Tp1ApplicationTest {

    @Autowired
    private MockMvc mockMvc;


/*    @Before
    public void setUp() {
        UserRestControleur controller = new UserRestControleur(); // 1
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build(); // 2
    }*/

    /**
     * renvoie la première page (le menu)
     * @throws Exception
     */
    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andReturn();
    }

    /**
     * Get all users
     * @throws Exception
     */
    @Test
    public void userAllShouldReturnMessageFromController() throws Exception{
        this.mockMvc.perform(get("/users")
                .header("Accept", "text/html"))
                .andExpect(status().isOk())
                .andReturn();
    }

    /**
     * get user
     * @throws Exception
     */
    @Test
    public void userShouldReturnMessageFromController() throws Exception{
        this.mockMvc.perform(get("/users/{login}","jonathan_T")
                    .header("Accept", "text/html"))
                    .andExpect(status().isOk())
                    .andReturn();
    }

    /**
     * create user
     * @throws Exception
     */
    @Test
    public void userPosted() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .param("login","test")
                .param("password", "test"))
                .andExpect(status().isCreated())
                .andReturn();

    }

    /**
     * update username
     * @throws Exception
     */
    /*@Test
    public void updateUserTest() throws Exception
    {
        mockMvc.perform( MockMvcRequestBuilders
                .put("/users/{login}","jonathan_T")
                .contentType(MediaType.APPLICATION_JSON)
                .content("\"password\"= \"" + "aaaa" + "\""))
                .andExpect(status().isOk());
    }*/

    /**
     * delete user
     * @throws Exception
     */
    @Test
    public void deleteUserTest() throws Exception
    {
        mockMvc.perform( MockMvcRequestBuilders
                .delete("/users/{login}", "jonathan_T"))
                .andExpect(status().isNoContent());
    }
}
