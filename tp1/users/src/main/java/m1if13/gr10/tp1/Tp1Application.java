package m1if13.gr10.tp1;

import org.springframework.context.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import m1if13.gr10.tp1.dao.*;
import m1if13.gr10.tp1.model.*;

@Configuration
@SpringBootApplication
public class Tp1Application extends SpringBootServletInitializer {
	//private static Dao<User> userDao;

	@Bean
	public UserDao userDao(){
		return new UserDao();
	}
	//@Autowired private User user;
	public static void main(String[] args) {
		
		SpringApplication.run(Tp1Application.class, args);
		ApplicationContext ctx = new AnnotationConfigApplicationContext(Tp1Application.class);
		UserDao userDao = ctx.getBean(UserDao.class);
		System.out.println(userDao.getAll());
		userDao.save(new User("a","b"));
		System.out.println(userDao.getAll());
		System.out.println(userDao.get("a"));
		User u = new User("abc","bcd");
		System.out.println(u.getLogin());
		//System.out.println(authenticate(""));
		System.out.println(userDao.getAll());
		u.setLogin("jeanduval");
		try {
			u.authenticate("bcd");
		  } catch (Exception e) {
			System.out.println("Something went wrong.");
		  }

		try {
			u.authenticate("password");
		  } catch (Exception e) {
			System.out.println("Something went wrong.");
		  }
		System.out.println(userDao.getAll());
		userDao.update(u, new String[]{"login", "mdp"});
		System.out.println(u.getLogin());
		System.out.println(userDao.getAll());
		userDao.delete(u);
		System.out.println(userDao.getAll());
		System.out.println(userDao.get("k"));
		((ConfigurableApplicationContext)ctx ).close();

	}

	/*public static final String MEDIA_TYPE_JSON  = "application/json";

	@EnableWebMvc
	public class WebConfig implements WebMvcConfigurer {
		@Override
		public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
			configurer.defaultContentType(MediaType.valueOf(MEDIA_TYPE_JSON));
		}
	}*/


	/*private static User getUser(String id) {
        Optional<User> user = userDao.get(id);
        
        return user.orElseGet(
          () -> new User("non-existing user", "no-email"));
    }*/

	/*public void run(String... args) {
        logger.info(user.getLogin());
		System.out.println(user.getLogin());
		System.out.println("\n");
		System.out.println(user.isConnected());
    }*/

}
