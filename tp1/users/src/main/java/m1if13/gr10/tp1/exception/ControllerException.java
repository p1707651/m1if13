package m1if13.gr10.tp1.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ControllerException {
    private static String ERROR_PAGE = "error";

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserException.class)
    public ModelAndView handleUserException(UserException userexception, HttpServletRequest request){
        verifException(userexception);

        ModelAndView view = new ModelAndView();
        view.addObject("userexception", userexception.getMessage());
        view.addObject("url", request.getRequestURL());
        view.setViewName(ERROR_PAGE);
        return view;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(PasswordException.class)
    public ModelAndView handlePasswordException(PasswordException passwordexception, HttpServletRequest request){
        verifException(passwordexception);

        ModelAndView view = new ModelAndView();
        view.addObject("passwordexception", passwordexception.getMessage());
        view.addObject("url", request.getRequestURL());
        view.setViewName(ERROR_PAGE);
        return view;
    }


    private void verifException(RuntimeException exception){
        if (AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class) != null){
            throw exception;
        }
    }
}
