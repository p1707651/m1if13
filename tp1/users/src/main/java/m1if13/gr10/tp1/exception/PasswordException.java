package m1if13.gr10.tp1.exception;

public class PasswordException extends RuntimeException{

    public PasswordException(){
        super("Mauvais mot de passe");
    }
    
}
