package m1if13.gr10.tp1.dao;

import m1if13.gr10.tp1.model.User;

import java.util.*;

public class UserDao implements Dao<User> {
    
    private Map<String,User> users = new HashMap<>();

    public UserDao(){
        users.put("jonathan_T", new User("jonathan_T","jjjj"));
    }

    /**
     * Récupère un utilisateur
     * @param login Login de l'utilisateur
     * @return Un java.util.Optional qui contient (ou pas) l'utilisateur
     */
    @Override
    public Optional<User> get(String login){
        return Optional.ofNullable(users.get(login));
    }

    /**
     * Récupère tous les utilisateurs
     * @return Un Set de login
     */
    @Override
    public Set<String> getAll(){
        return users.keySet();
    }

    /**
     * Crée un utilisateur et le sauvegarde
     * @param user L'utilisateur à créer
     */
    @Override
    public void save(User user){
        users.put(user.getLogin(),user);
    }

    /**
     * Modifie un utilisateur
     * @param user L'utilisateur à modifier
     * @param params Un tableau de **2** Strings : login et password
     */
    @Override
    public void update(User user, String[] params){
        user.setLogin(Objects.requireNonNull(params[0],"Le Login ne peut pas être null"));
        user.setPassword(Objects.requireNonNull(params[1],"Le Password ne peut pas être null"));
        users.put(user.getLogin(),user);
    }

    /**
     * Supprime un utilisateur
     * @param user L'utilisateur à supprimer
     */
    @Override
    public void delete(User user){
        users.remove(user.getLogin());
    }
}
