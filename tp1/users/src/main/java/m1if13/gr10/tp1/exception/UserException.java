package m1if13.gr10.tp1.exception;



public class UserException extends RuntimeException {
    
    public UserException(String login, boolean connect){
        super(String.format("User %s est " + (connect? "déjà connecter": "pas connecter"), login));
    }
}