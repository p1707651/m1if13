package m1if13.gr10.tp1.controller;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import m1if13.gr10.tp1.model.User;
import m1if13.gr10.tp1.dao.UserDao;
import m1if13.gr10.tp1.utils.Jwt;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

@Controller
@CrossOrigin(origins = {"http://localhost", "http://192.168.75.107", "https://192.168.75.107", "http://localhost:4000"})
public class OperationController extends Jwt{

    @Autowired
    private UserDao userDao;

    /**
     * Procédure de login "simple" d'un utilisateur
     * @param login Le login de l'utilisateur. L'utilisateur doit avoir été créé préalablement et son login doit être présent dans le DAO.
     * @param password Le password à vérifier.
     * @return Une ResponseEntity avec le JWT dans le header "Authentication" si le login s'est bien passé, et le code de statut approprié (204, 401 ou 404).
     */
    @Operation(summary = "connection utilisateur",
            description = "generation du token si l'utilisateur existe",
            responses = {
                @ApiResponse(responseCode = "204", description = "NO CONTENT", content = @Content(mediaType = "application/json")),
                @ApiResponse(responseCode = "401", description = "UNAUTHORIZED"),
                @ApiResponse(responseCode = "404", description = "NO FOUND")
            })
    @PostMapping("/login")
    public ResponseEntity<Void> login(@RequestParam("login") String login, @RequestParam("password") String password, @RequestHeader("Origin") String origin) {
        if(login.isEmpty() || password.isEmpty()){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        else if(userDao.getAll().contains(login)){
            HttpHeaders headers = new HttpHeaders();
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String token = generateToken(login, request);
            headers.add("Authorization", token);
            headers.add("Access-Control-Expose-Headers", "Authorization");
            try {
                userDao.get(login).get().authenticate(password);
            } catch (Exception e) {
                System.out.println("Something went wrong.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(headers,HttpStatus.NO_CONTENT);
        }   
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Réalise la déconnexion
     */
    @Operation(summary = "Logout User", responses = {
            @ApiResponse(
                    description = "Déconnecte lorsque le token est trouvé",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = String.class)))})
    @DeleteMapping("/logout")
    public ResponseEntity<Void> logout(@RequestParam("token") String token, @RequestHeader("Origin") String origin) {
        if(token.isEmpty()){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        else{
            HttpHeaders headers = new HttpHeaders();
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String notoken = verifyToken(token, request);
            if(userDao.get(notoken).isPresent()){
                Optional <User> u = userDao.get(notoken);
                User u1 = u.get();
                u1.disconnect();
                headers.add("Authentication", u1.getLogin());
                return new ResponseEntity<>(headers,HttpStatus.NO_CONTENT);
            }else{
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }
    }


    /**
     * Méthode destinée au serveur Node pour valider l'authentification d'un utilisateur.
     * @param token Le token JWT qui se trouve dans le header "Authentication" de la requête
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @Operation(summary = "Authenticate User", responses = {
            @ApiResponse(description = "Connexion avec un token", responseCode = "200", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Not found"),
            @ApiResponse(responseCode = "401", description = "Authentication Failure") })
    @GetMapping("/authenticate")
    public ResponseEntity<Void> authenticate(@RequestParam("token") String token, @RequestParam("origin") String origin) {
        if(token.isEmpty()){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        else{
            HttpHeaders headers = new HttpHeaders();
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String notoken = verifyToken(token, request);
            if(userDao.getAll().contains(notoken)){
                headers.add("Authentication", notoken);
                return new ResponseEntity<>(headers,HttpStatus.NO_CONTENT);
            }else{
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }
    }

}
