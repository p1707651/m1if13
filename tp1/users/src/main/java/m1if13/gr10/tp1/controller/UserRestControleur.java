package m1if13.gr10.tp1.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.*;

import m1if13.gr10.tp1.dao.UserDao;
import m1if13.gr10.tp1.model.User;
import m1if13.gr10.tp1.utils.Jwt;
import m1if13.gr10.tp1.exception.UserException;

@RestController
public class UserRestControleur extends Jwt{
    
    @Autowired
    private UserDao userDao;

    @Operation(summary = "Get all users", description = "Récupère la liste de tout les utilisateurs", responses =
            @ApiResponse(responseCode = "200", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = User.class)),
                    @Content(mediaType = "application/xml", schema = @Schema(implementation = User.class)),
                    @Content(mediaType = "text/html", schema = @Schema(description = "Tout les utilisateur"))
            }))
    @GetMapping(value = "/users", produces = { "application/json", "application/xml" })
    @ResponseBody
    public Set<String> getAllUsers() {
        return userDao.getAll();
    }

    @Operation(summary = "Get user", description = "Récupère les informations d'un utilisateur", responses =
    @ApiResponse(responseCode = "200", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = User.class)),
            @Content(mediaType = "application/xml", schema = @Schema(implementation = User.class)),
            @Content(mediaType = "text/html", schema = @Schema(description = "information d'un utilisateur"))
    }))
    @GetMapping(value = "/users/{login}", produces = { "application/json", "application/xml" })
    @ResponseBody
    public User getUser(@PathVariable("login") @Schema(example = "jonathan_T") String login) {
        //si existe
        Optional<User> user = userDao.get(login);

        return user.get();
    }

    @GetMapping(value = "/users/{login}", produces = "text/html")
    public ModelAndView getUserHtml(@PathVariable("login") @Schema(example = "jonathan_T") String login) {
        //Check if the user exists
        Optional<User> user = userDao.get(login);
        if(user.isEmpty()){
            throw new UserException(login, false);
        }

        ModelAndView mav = new ModelAndView("updateuser");
        mav.addObject("updateuser", user.get().getLogin());
        return mav;
    }

    
    /*@RequestMapping(value="/user", method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Set<String> getAllUserBody(Model model) {
        Set<String> user = getAll();
        model.addAttribute("user",user);
        //return getAll();
        return "user";
    }*/
    
    /*@GetMapping(value = "/", produces = { "application/json", "application/xml" })
    @ResponseBody
    public Set<String> getAllUsers() {
        Set<String> user = userDao.getAll();
        return user;
    }*/

    @GetMapping(value="/", produces = "text/html")
    @ResponseBody
    public ModelAndView mainPage(){
        /*return "menu";*/
        ModelAndView mav = new ModelAndView("menu");
        /*mav.addObject("menu", userDao.getAll());*/
        return mav;
    }

    @GetMapping(value="/login",produces = "text/html")
    @ResponseBody
    public ModelAndView loginform() {
        ModelAndView mav = new ModelAndView("login");
        return mav;
    }

    @GetMapping(value="/users",produces = "text/html")
    @ResponseBody
    public ModelAndView addform() {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", userDao.getAll());
        return mav;
    }

    @GetMapping(value="/logout",produces = "text/html")
    public ModelAndView logout(ModelMap model,HttpSession session) {
        ModelAndView mav = new ModelAndView("menu");
        userDao.get((String)session.getAttribute("login")).get().disconnect();
        session.invalidate();
        return mav;
    }

    @PostMapping(value="/profil")
    //@ResponseStatus(HttpStatus.OK)
    public ModelAndView login(@RequestParam(value="login", required=false) String login, 
                            @RequestParam(value="password", required=false) String password,
                            ModelMap model,HttpSession session) {
        if(login.isEmpty() || password.isEmpty()){
            new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            ModelAndView mav = new ModelAndView("login");
            return mav;
        }
        else if(userDao.getAll().contains(login)){
            HttpHeaders headers = new HttpHeaders();
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String token = generateToken(login, request);
            headers.add("Authentication", token);
            try {
                userDao.get(login).get().authenticate(password);
            } catch (Exception e) {
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
                ModelAndView mav = new ModelAndView("login");
                return mav;
            }
            //model.addAttribute("login",login);
            model.addAttribute("password",password);
            session.setAttribute("login", login);
            new ResponseEntity<>(headers,HttpStatus.NO_CONTENT);
            
        }   
        //Set<String> user = userDao.getAll();
        //return getAll();
        ModelAndView mav = new ModelAndView("profil");
        return mav;
        
    }

    @Operation(summary = "Add user", description = "Creation du nouveau utilisateur", responses = {
        @ApiResponse(responseCode = "201", description = "OK"),
        @ApiResponse(responseCode = "404", description = "Utilisateur exisant")
    })
    @PostMapping(value="/users", produces = "application/json")
    //@ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Void> AddUser(@RequestParam(value="login", required=false) String login, 
                            @RequestParam(value="password", required=false) String password) {
        Optional<User> user = userDao.get(login);

        if(user.isPresent()){
            throw new UserException(login,true);
        }

        userDao.save(new User(login,password));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @Operation(summary = "Update user", description = "Modification du mot de passe de l'utilisateur", responses = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "404", description = "Aucun utilisateur avec ce login")
    })
    @PutMapping(value="/users/{login}")
    public ResponseEntity<Void> updateUser(@PathVariable("login") @Schema(example = "jonathan_T") String login, 
                            @RequestParam(value="password", required=false) String password,
                            ModelMap model,HttpSession session) {                       
        session.setAttribute("login", login);
        model.addAttribute("password",password);
        Optional <User> user = userDao.get(login);
        if(user.isEmpty()){
            throw new UserException(login,false);
        }
        User userupdate = user.get();
        userDao.update(userupdate,new String[]{login,password});

        //ModelAndView mav = new ModelAndView("profil");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Delete user", description = "Suppression d'un utilisateur existant", responses = {
        @ApiResponse(responseCode = "204", description = "No Content"),
        @ApiResponse(responseCode = "404", description = "Aucun utilisateur avec ce login")
    })
    @DeleteMapping(value = "/users/{login}")
    public ResponseEntity<Void> deleteUser(@PathVariable("login") @Schema(example = "jonathan_T") String login) {
        Optional<User> user = userDao.get(login);
        if (user.isEmpty()){
            throw new UserException(login, false);
        }
        userDao.delete(user.get());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
