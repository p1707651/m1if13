// vue.config.js

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */

module.exports = {
  devServer: {
    port: 4000,
    proxy: {
      "/V1": {
        target: "http://192.168.75.107:8080/TP1",
        changeOrigin: true,
        pathRewrite: {
          "^/V1": "",
        },
      },
      "/V2": {
        target: "http://192.168.75.107:3376",
        changeOrigin: true,
        pathRewrite: {
          "^/V2": "",
        },
      },
    },
  },
  pwa: {
    name: 'GR10 App',
    themeColor: '#4DBA87',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: "src/sw.js",
      // ...other Workbox options...
    },
  },
  configureWebpack: {
    plugins: [
      //vide pour le
    ],
  },
};
