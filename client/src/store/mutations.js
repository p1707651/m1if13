const mutations = {
  updateUser(state, data) {
    state.ttl = 120;
    state.playerPosition = data.position;
  },
  updatePlayerPosition(state, position) {
    state.playerPosition = position;
  },
  decreaseTtl(state) {
    state.ttl--;
  },
  setZrr(state, res) {
    state.zrr = res;
  },
  setMeteorites(state, meteorites) {
    state.meteorite = meteorites;
  },
  clear(state) {
    state.ttl = null;
    state.playerPosition = null;
    state.meteorite = [];
    state.zrr = [];
    state.game = false;
  },
  setStartGame(state, value) {
    state.game = value;
  },
  setTtl(state, value) {
    state.ttl = value;
  },
  deleteMeteorite(state, index) {
    state.meteorite.splice(index, 1);
  },
};

export default mutations;
