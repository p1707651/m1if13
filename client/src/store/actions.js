import {
  apiLogin,
  apiGetUser,
  apiUpdatePlayerPositions,
  apiGetPlayerPositions,
  apiGetMeteorites,
  apiLogout,
  apiGetAllUsers,
  apiDeleteMeteorites,
} from "../api/game";
import router from "../router";

const actions = {
  login({ dispatch }, datas) {
    // datas contains login and password values
    apiLogin(datas.login, datas.password)
      .then((resp) => {
        console.log(resp);
        localStorage.setItem("token", resp.headers.authorization);
        dispatch("getUser", datas.login);
        dispatch("getAllUsers"),
        dispatch("getAllMeteorites");
        dispatch("startGame");
        dispatch("getPlayerPositions", datas.login);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  logout({ commit }) {
    apiLogout(localStorage["token"].slice(7))
      .then((resp) => {
        console.log(resp);
        localStorage.clear();
        commit("clear");
        router.push("login");
      })
      .catch((error) => {
        console.log(error);
      });
  },
  getUser({ commit }, login) {
    apiGetUser(login, localStorage["token"].slice(7))
      .then((resp) => {
        console.log("ici man");
        console.log(resp);
        localStorage.setItem("login", resp.data[0].survivant);
        localStorage.setItem(
          "image",
          resp.data[0].image === null ? "" : resp.data[0].image
        );
        localStorage.setItem("iduser", resp.data[0].id);
        if (resp.status !== 404) {
          localStorage.userInfo = JSON.stringify(resp.data[0]);
          commit("updateUser", resp.data);
          router.push("profil");
        } else {
          router.push("createuser");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  },
  getMenu(){
    router.push("profil");
  },
  getAllUsers(){
    apiGetAllUsers(localStorage["token"].slice(7))
    .then((resp) => {
      console.log("ici man");
      console.log(resp);
      if (resp.status !== 404) {
        localStorage.userArray = JSON.stringify(resp.data);
      }
    })
    .catch((error) => {
      console.log(error);
    });
  },
  getPlayerPositions({ commit }, login) {
    apiGetPlayerPositions(login, localStorage["token"].slice(7))
      .then((resp) => {
        console.log("ici man 2");
        console.log(resp);
        localStorage.setItem("position", resp.data[0].position);
        commit("updatePlayerPosition", resp.data);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  updatePlayerPositions({ commit }, position) {
    apiUpdatePlayerPositions(
      localStorage.getItem("iduser"),
      position,
      localStorage["token"].slice(7)
    )
      .then((resp) => {
        console.log(resp);
        commit("updatePlayerPosition", position);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  decreaseTtlAction({ commit }) {
    commit("decreaseTtl");
  },
  getAllMeteorites({ commit }) {
    apiGetMeteorites(localStorage["token"].slice(7))
      .then((resp) => {
        let res = new Array();
        for (let i in resp.data) {
          console.log("saluuuuut", resp.data[i]);
          for (let j = 0; j < resp.data[i].length; j++) {
            if (resp.data[i][j].composition !== undefined) {
              console.log(resp.data[i][j].ttl);
              res.push(resp.data[i][j]);
            }
          }
        }
        commit("setMeteorites", res);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  startGame({ commit }) {
    commit("setStartGame", true);
  },
  resetTtl({ commit }) {
    commit("setTtl", null);
  },
  playerImpact({ commit, state }, datas) {
    var meteorite = datas.meteorite;
    var index = datas.index;
    var meteorites = datas.meteorites;
    var mymap = datas.mymap;
    var ttl = parseInt(state.ttl, 10) + parseInt(meteorite.ttl, 10);

    console.log("juste pour essayer de voir", meteorite.id);

    commit("setTtl", ttl);
    commit("deleteMeteorite", index);

    apiDeleteMeteorites(meteorite.id,localStorage["token"].slice(7)
    ).then((resp) => {
      console.log(resp);
    })
    .catch((error) => {
      console.log(error);
    });


    meteorites[index].remove(mymap);
    meteorites.splice(index, 1);
  },
};

export default actions;
