import axios from "axios";

export function apiLogin(login, password) {
  return axios.post("https://192.168.75.107/api/TP1/login", null, {
    params: {
      login: login,
      password: password,
    },
  });
}

export function apiLogout(token) {
  return axios.delete("https://192.168.75.107/api/TP1/logout", {
    headers: {
      Authorization: token,
    },
  });
}

export function apiGetUser(login, token) {
  return axios.get(`https://192.168.75.107/game/api/resources/user/${login}`, {
    headers: {
      Authorization: token,
    },
  });
}

export function apiGetAllUsers(token) {
  return axios.get(`https://192.168.75.107/api/TP1/users`,{
    headers: {
      Authorization: token,
    },
  });
}

export function apiUpdateImage(login, image, token) {
  return axios.put(
    `https://192.168.75.107/game/api/resources/${login}/image`,
    {
      image: image,
    },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export function apiUpdatePlayerPositions(id, position, token) {
  return axios.put(
    `https://192.168.75.107/game/api/resources/${id}/position`,
    {
      position: position,
    },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export function apiUpdatePlayerTTL(login, ttl, token) {
  return axios.put(
    `https://192.168.75.107/game/api/resources/user/${login}`,
    {
      TTL: ttl - 1,
    },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export function apiGetPlayerPositions(login, token) {
  return axios.get(
    `https://192.168.75.107/game/api/resources/user/${login}/position`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export function apiGetMeteorites(token) {
  return axios.get(`https://192.168.75.107/game/api/resources/meteorites`, {
    headers: {
      Authorization: token,
    },
  });
}

export function apiDeleteMeteorites(id,token) {
  return axios.post(`https://192.168.75.107/game/api/resources/meteorite/${id}`, {
    headers: {
      Authorization: token,
    },
  });
}


