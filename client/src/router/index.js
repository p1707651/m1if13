import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/histoire",
    name: "Histoire",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Histoire.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../components/Login.vue"),
  },
  {
    path: "/map",
    name: "Map",
    component: () => import("../components/Map.vue"),
  },
  {
    path: "/createuser",
    name: "CreateUser",
    component: () => import("../views/CreateUser.vue"),
  },
  {
    path: "/profil",
    name: "Profil",
    component: () => import("../views/Profil.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
