# M1IF13

## ETUDIANTS

|   Nom |   Prénom  |   N°étudiant  |
|------------------|:-----------:|:------------:|
| Talcone | Jonathan | 11707651 |
| Askia | Kader | 11609837 |

## TP1 & TP2

liens:

[application HTML](http://192.168.75.107:8080/TP1/)

[swagger-ui.html](http://192.168.75.107:8080/TP1/swagger-ui.html)

[apidocs (lien page web)](http://192.168.75.107:8080/TP1/v3/api-docs)

[apidocs (lien répertoire projet)](https://forge.univ-lyon1.fr/p1707651/m1if13/-/blob/master/tp1/users-api.yaml)

[répertoire users](https://forge.univ-lyon1.fr/p1707651/m1if13/-/tree/master/tp1/users)

[Répertoire simple-users (view thymeleaf)](https://forge.univ-lyon1.fr/p1707651/m1if13/-/tree/tp1/tp1/users/src/main/resources/simple-users)

## TP3 & TP4 

liens:

[application admin](http://192.168.75.107:3376/game/admin)

[application client](https://192.168.75.107/)
